﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VitaliyTask
{
    public class DoublyLinkedList<T>
    {
        private T[] array;
        public int Length
        {
            get
            {
                return array.Length;
            }
        }

        // Инициализация пустого массива, иначе программа может скрашиться.
        public DoublyLinkedList()
        {
            array = new T[0];
        }

        // Nested-класс
        public class Node
        {
            // Ссылка на внешний класс для использования его данных.
            private DoublyLinkedList<T> outer;

            // Позиция текущего узла в списке.
            private int index;

            public T Value { get; set; }

            // Конструктор, принимающий только ссылку на внешний класс. Пригоден для внутреннего использования.
            public Node(DoublyLinkedList<T> outer)
            {
                this.outer = outer;
            }

            // Конструктор, задающий индекс и, соответственно, значение. Может использоваться везде.
            public Node(DoublyLinkedList<T> outer, int index)
            {
                this.outer = outer;
                this.index = index;
                Value = outer[index];
            }

            public Node Next
            {
                get
                {
                    var i = index + 1;
                    if (i < outer.Length) return new Node(outer)
                    {
                        index = i,
                        Value = outer[i]
                    };
                    return null;
                }
            }

            public Node Previous
            {
                get
                {
                    var i = index - 1;
                    if (i >= 0) return new Node(outer)
                    {
                        index = i,
                        Value = outer[i]
                    };
                    return null;
                }
            }

        }

        public Node Head
        {
            get
            {
                // Первый узел списка.
                return new Node(this, 0);
            }
        }

        public Node Tail
        {
            get
            {
                // Последний узел списка.
                return new Node(this, array.Length-1);
            }
        }
        
        // Индексатор.
        public T this[int index]
        {
            get { return array[index]; }
            set { array[index] = value; }
        }

        // Добавляем в конец.
        public void Add(T value)
        {
            array = array.Append(value).ToArray();
        }

        // Добавляем в начало.
        public void AddHead(T value)
        {
            // Инициализация нового массива с единственным элементом, равным value. Конкатенируем с имеющимся массивом.
            array = new T[] { value }.Concat(array).ToArray();
        }

        public void Delete(int index)
        {
            // Фильтрация, в которой выбираем все элементы, кроме элемента с индексом index.
            array = array.Where((_, i) => i != index).ToArray();
        }

    }
}
