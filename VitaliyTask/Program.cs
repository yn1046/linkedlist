﻿using System;

namespace VitaliyTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new DoublyLinkedList<int>();
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.AddHead(8);
            list.Head.Next.Value = 228;

            list.Delete(3);

            for (var Cursor = list.Head; Cursor != null; Cursor = Cursor.Next)
            {
                Console.WriteLine(Cursor.Value);                
            }
            Console.WriteLine("Length: " + list.Length);
        }
    }
}
